﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FarmaciAPI.Datos;
using FarmacyAPI.Entidades;
using Microsoft.Extensions.Logging;
using FarmacyAPI.Services.Contracts;
using FarmacyAPI.Web.ViewModels.Usuarios;

namespace FarmacyAPI.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsuariosController : ControllerBase
    {
        private readonly FarmacyAPIDbContext _context;
        private readonly ILoggerService _logger;

        public UsuariosController(FarmacyAPIDbContext context, ILoggerService logger)
        {
            _context = context;
            _logger = logger;
        }

        // GET: api/Usuarios/GetUsuarios
        [HttpGet("[action]")]
        public async Task<IEnumerable<UsuarioViewModel>> GetUsuarios()
        {
            var usuarios = await _context.Usuarios.Include(u => u.Permisos).ToListAsync();

            _logger.logInfo("Se ha visualizado la data sensible de los usuarios");

            return usuarios.Select(u => new UsuarioViewModel 
            { 
                idUsuario = u.idUsuario,
                permiso = u.Permisos.descripcion,
                nombreUsuario = u.nombreUsuario,
                nombreCompleto = u.nombreCompleto,
                email = u.email,
                state = u.state
            });
        }

        // GET: api/Usuarios/Usuario/5
        [HttpGet("[action]/{id}")]
        public async Task<IActionResult> Usuario([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var usuario = await _context.Usuarios.Include(u => u.Permisos).SingleOrDefaultAsync(u=>u.idPermiso == id);

            if (usuario == null)
            {
                return NotFound();
            }

            return Ok(new UsuarioViewModel 
            {
                idUsuario = usuario.idUsuario,
                permiso = usuario.Permisos.descripcion,
                nombreUsuario = usuario.nombreUsuario,
                nombreCompleto = usuario.nombreCompleto,
                email = usuario.email,
                state = usuario.state
            });
        }

        // PUT: api/Usuarios/UpdateUsuario/5
        [HttpPut("[action]/{id}")]
        public async Task<IActionResult> UpdateUsuario ([FromRoute] int id, [FromBody] UpdateUsuarioViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (model == null)
            {
                return BadRequest();
            }

            var usuario = await _context.Usuarios.FindAsync(id);

            if(model.contrasena == model.confirmaContrasena)
            {
                usuario.idPermiso = model.idPermiso;
                usuario.nombreCompleto = model.nombreCompleto;
                usuario.nombreUsuario = model.nombreUsuario;
                usuario.email = model.email;

                try
                {
                    await _context.SaveChangesAsync();
                    _context.Entry(usuario).State = EntityState.Modified;
                }
                catch (Exception ex)
                {
                    if (!UsuarioExists(id))
                    {
                        _logger.logWarn("El usuario no pudo ser encontrado para la actualización");
                        return BadRequest();
                    }
                    else
                    {
                        string messagee = string.Empty;

                        if (ex.InnerException != null)
                            messagee = ex.InnerException.Message;
                        else
                            messagee = ex.Message;

                        _logger.logError(messagee);
                        return BadRequest();
                    }
                }
            }

            return Ok(model);
        }

        // POST: api/Usuarios/InsertUsuario
        [HttpPost("[action]")]
        public async Task<IActionResult> InsertUsuario([FromBody] InsertUsuarioViewModel model )
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if(model.contrasena == model.confirmaContrasena)
            {
                try
                {
                    Usuario usuario = new Usuario
                    {
                        idPermiso = model.idPermiso,
                        nombreCompleto = model.nombreCompleto,
                        nombreUsuario = model.nombreUsuario,
                        email = model.email,
                        contraseña = model.contrasena,
                        state = true
                    };

                    _context.Usuarios.Add(usuario);
                    await _context.SaveChangesAsync();
                }
                catch (Exception ex)
                {
                    string messagee = string.Empty;

                    if (ex.InnerException != null)
                        messagee = ex.InnerException.Message;
                    else
                        messagee = ex.Message;

                    _logger.logError(messagee);

                    return BadRequest();
                }
            }
            else
            {
                _logger.logWarn("La contraseña y la validación no coinciden. Favor verificar");
                return BadRequest();
            }

            return Ok(model);
        }

        // DELETE: api/Usuarios/5
        [HttpPost("[action]/{id}")]
        public async Task<IActionResult> DesactivarUsuario([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var usuario = await _context.Usuarios.FindAsync(id);

            if (usuario == null)
            {
                return NotFound();
            }

            usuario.state = false;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                string messagee = string.Empty;

                if (ex.InnerException != null)
                    messagee = ex.InnerException.Message;
                else
                    messagee = ex.Message;

                _logger.logError(messagee);
                throw;
            }

            return Ok();
        }

        // DELETE: api/Usuarios/5
        [HttpPost("[action]/{id}")]
        public async Task<IActionResult> ActivarUsuario([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var usuario = await _context.Usuarios.FindAsync(id);

            if (usuario == null)
            {
                return NotFound();
            }

            usuario.state = true;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                string messagee = string.Empty;

                if (ex.InnerException != null)
                    messagee = ex.InnerException.Message;
                else
                    messagee = ex.Message;

                _logger.logError(messagee);
            }

            return Ok();
        }

        private bool UsuarioExists(int id)
        {
            return _context.Usuarios.Any(e => e.idUsuario == id);
        }
    }
}
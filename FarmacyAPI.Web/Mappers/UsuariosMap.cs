﻿using FarmacyAPI.Entidades;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace FarmaciAPI.Datos.Mappers
{
    public class UsuariosMap : IEntityTypeConfiguration<Usuario>
    {
        public void Configure(EntityTypeBuilder<Usuario> builder)
        {
            builder.ToTable("Usuarios")
                .HasKey(u => u.idUsuario);
            builder.HasOne(p => p.Permisos)
                .WithMany(p => p.Usuarios)
                .HasForeignKey(p => p.idPermiso);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FarmacyAPI.Entidades
{
    public class Permiso
    {
        public int idPermiso { get; set; }
        public string descripcion { get; set; }

        public virtual ICollection<Usuario> Usuarios { get; set; }
    }
}

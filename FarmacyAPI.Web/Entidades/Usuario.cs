﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FarmacyAPI.Entidades
{
    public class Usuario
    {
        public int idUsuario { get; set; }
        public int idPermiso { get; set; }
        public string nombreUsuario { get; set; }
        public string nombreCompleto { get; set; }
        public string email { get; set; }
        public string contraseña { get; set; }
        public bool state { get; set; }

        public virtual Permiso Permisos { get; set; }
        public virtual OrderHeader OrderHeader { get; set; }
    }
}

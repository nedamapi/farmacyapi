﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FarmacyAPI.Entidades
{
    public class OrderDetail
    {
        public int idOrderDetail { get; set; }
        public int idOrderHeader { get; set; }
        public int idProducto { get; set; }
        public int cantidad { get; set; }
        public decimal precioUnidad { get; set; }
        public decimal precioTotal { get; set; }
        public decimal itbis { get; set; }

        public virtual OrderHeader OrderHeader { get; set; }
        public virtual ICollection<Producto> Productos { get; set; }
    }
}

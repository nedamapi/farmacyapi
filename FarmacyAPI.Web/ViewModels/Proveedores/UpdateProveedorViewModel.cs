﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FarmacyAPI.Web.ViewModels.Proveedores
{
    public class UpdateProveedorViewModel
    {
        public string nombre { get; set; }
        public string telefono { get; set; }
        public string direccion { get; set; }
        public string cedula { get; set; }
        public string rnc { get; set; }
    }
}

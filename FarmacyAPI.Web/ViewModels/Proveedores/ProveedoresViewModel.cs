﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FarmacyAPI.Web.ViewModels.Proveedores
{
    public class ProveedoresViewModel
    {
        [Key]
        public int idProveedor { get; set; }
        [DisplayName("Nombre")]
        public string nombre { get; set; }
        [DisplayName("Telefono")]
        [Phone]
        public string telefono { get; set; }
        [DisplayName("Direccion")]
        public string direccion { get; set; }
        [DisplayName("Cedula")]
        public string cedula { get; set; }
        [DisplayName("RNC")]
        public string rnc { get; set; }
    }
}

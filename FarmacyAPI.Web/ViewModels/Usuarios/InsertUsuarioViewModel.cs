﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FarmacyAPI.Web.ViewModels.Usuarios
{
    public class InsertUsuarioViewModel
    {
        [Key]
        public int idPermiso { get; set; }
        public string nombreUsuario { get; set; }
        public string nombreCompleto { get; set; }
        public string contrasena { get; set; }
        public string confirmaContrasena { get; set; }
        [EmailAddress]
        public string email { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FarmacyAPI.Web.ViewModels.Usuarios
{
    public class UsuarioViewModel
    {
        [Key]
        public int idUsuario { get; set; }
        public string permiso { get; set; }
        [DisplayName("Nombre de Usuario")]
        public string nombreUsuario { get; set; }
        [DisplayName("Nombre Completo")]
        public string nombreCompleto { get; set; }
        [EmailAddress]
        public string email { get; set; }
        public bool state { get; set; }
    }
}

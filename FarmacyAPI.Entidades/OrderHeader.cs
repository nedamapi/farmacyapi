﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FarmacyAPI.Entidades
{
    public class OrderHeader
    {
        public int idOrderHeader { get; set; }
        public int idUsuario { get; set; }
        public decimal subTotal { get; set; }
        public decimal totalVenta { get; set; }
        public DateTime fechaVenta { get; set; }
        public int posicion { get; set; }


        public virtual Usuario Usuarios { get; set; }
        public virtual ICollection<OrderDetail> OrderDetails { get; set; }
    }
}

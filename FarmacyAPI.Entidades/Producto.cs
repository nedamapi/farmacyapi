﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FarmacyAPI.Entidades
{
    public class Producto
    {
        public int idProducto { get; set; }
        public int idProveedor { get; set; }
        public string descripcion { get; set; }
        public decimal precio { get; set; }
        public int stock { get; set; }
        public DateTime fechaVencimiento { get; set; }
        public bool state { get; set; }

        public virtual Proveedor Proveedores { get; set; }
        public virtual OrderDetail OrderDetails { get; set; }
    }
}

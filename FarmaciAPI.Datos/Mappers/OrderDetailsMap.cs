﻿using FarmacyAPI.Entidades;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace FarmaciAPI.Datos.Mappers
{
    public class OrderDetailsMap : IEntityTypeConfiguration<OrderDetail>
    {
        public void Configure(EntityTypeBuilder<OrderDetail> builder)
        {
            builder.ToTable("OrderDetail")
                .HasKey("idOrderDetail");
            builder.HasOne(od => od.OrderHeader)
                .WithMany(od => od.OrderDetails)
                .HasForeignKey(od => od.idOrderHeader);
            builder.HasMany(od => od.Productos)
                .WithOne(od => od.OrderDetails)
                .HasForeignKey(od => od.idProducto);
        }
    }
}
